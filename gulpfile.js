'use strict';

const gulp = require('gulp');
const watch = require('gulp-watch');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const minify = require('gulp-js-minify');
const imagemin = require('gulp-imagemin');
const eslint = require('gulp-eslint');
const server = require('gulp-server-livereload');

gulp.task('sass', () => {
    return gulp.src('./src/scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./includes/css/'))
        .pipe(rename({
            basename: 'style.min'
        }))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest('./includes/css/'))
});


gulp.task('js', () => {
    return gulp.src('./src/js/main.js')
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('./includes/js/'))
        .pipe(minify())
        .pipe(rename({
            basename: 'main.min'
        }))
        .pipe(gulp.dest('./includes/js/'))
});

gulp.task('imgcompress', () => {
    gulp.src('./images/*')
        .pipe(imagemin([
            imagemin.gifsicle({
                interlaced: true,
            }),
            imagemin.jpegtran({
                progressive: true,
            }),
            imagemin.optipng({
                optimizationLevel: 6
            }),
            imagemin.svgo({
                plugins: [
                    {
                        removeViewBox: true
                    },
                    {
                        cleanupIds: false
                    }
                ]
            })
        ]))
        .pipe(gulp.dest('./images/'))
});

gulp.task('webserver', ['watch'], function () {
    gulp.src('./')
        .pipe(server({
            port: 8888,
            defaultFile: 'index.html',
            livereload: {
                enable: true,
                filter: function (filename, cb) {
                    cb(!/\.(sa|le)ss$|node_modules/.test(filename));
                },
            },
            directoryListing: false, // true if defaultFile is empty
            open: true
        }));
});

gulp.task('watch', () => {
    gulp.watch('./src/scss/**/*.scss', ['sass']);
    gulp.watch('./src/js/main.js', ['js']);
});

gulp.task('default', ['sass', 'js', 'watch', 'webserver']);
gulp.task('prod', ['imgcompress']);