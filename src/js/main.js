'use strict';
//header menu
(() => {
    const headerMenuBtn = document.querySelector('.header__menu__hamburger');
    headerMenuBtn.addEventListener('click', function() {
        headerMenuBtn.classList.toggle('header__menu__hamburger--open');
    });
})();

//inputs active
const InputListener = (() => {
    const allInput = Array.prototype.slice.call(document.querySelectorAll('.form-control'));
    if(!allInput) {
        return;
    }

    allInput.map((input) => {
        input.addEventListener('change', function(e) {
            const { target } = e;
            if (!target.value) {
                if (!target.classList.contains('active')) {
                    return;
                }
                target.classList.remove('active'); 
            } else {
                target.classList.add('active');
            }
        });
    });
});

InputListener();

//auto submit
let form = document.getElementById('filters');

if (form != null) {
    form.addEventListener('change', function () {
        form.submit();
    });
}

let UTILS = {
    init: function () {
        //defaultowe klasy które zawsze muszą być uruchomione
        this.cookieDisclaimer.init();
    },
    hasClass: function(ele, cls) {
        return !!ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
    },
    addClass: function(ele, cls) {
        if (!UTILS.hasClass(ele, cls)) {
            ele.className += ' ' + cls;
        }
    },
    removeClass: function(ele, cls) {
        if (UTILS.hasClass(ele, cls)) {
            let reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
            ele.className = ele.className.replace(reg, ' ');
        }
    },
    cookie: {
        set: function (c_name, value, exdays) {
            let exdate = new Date();
            exdate.setDate(exdate.getDate() + exdays);
            let c_value = escape(value) + ((exdays == null) ? '' : '; path=/; expires=' + exdate.toUTCString());
            document.cookie = c_name + '=' + c_value;
        },
        get: function (c_name) {
            let i, x, y, ARRcookies = document.cookie.split(';');
            for (i = 0; i < ARRcookies.length; i++) {
                x = ARRcookies[i].substr(0, ARRcookies[i].indexOf('='));
                y = ARRcookies[i].substr(ARRcookies[i].indexOf('=') + 1);
                x = x.replace(/^\s+|\s+$/g, '');
                if (x == c_name) {
                    return unescape(y);
                }
            }
        }
    },
    cookieDisclaimer: {
        cookieTime: 360, //in days
        container: null,
        disclaimerText: 'Informujemy, że w Portalu Birofilia.org wykorzystujemy pliki cookies. Modyfikowanie, blokowanie i usuwanie plików cookies możliwe jest przy użyciu przeglądarki internetowej. Więcej informacji na temat plików cookies można znaleźć w <a href="#">Polityce cookie Birofilia.org.</a>',
        disclaimerClose: 'OK',
        init: function () {
            let that = this;
            if (UTILS.cookie.get('cookie-disclaimer')) {
                that.setCookie();
            } else {
                document.body.innerHTML += '<div id="cookie-disclaimer"><div class="cookie-disclaimer-wrapper"><button class="cookie-disclaimer-close">' + this.disclaimerClose + '</button><div class="cookie-disclaimer-content">' + this.disclaimerText + '</div></div></div>';
                document.getElementsByClassName('cookie-disclaimer-close')[0].addEventListener('click', function() {
                    that.close();
                });

                UTILS.addClass(document.getElementById('cookie-disclaimer'), 'active');
            }
        },
        close: function () {
            this.setCookie();
            UTILS.removeClass(document.getElementById('cookie-disclaimer'), 'active');
        },
        setCookie: function () {
            UTILS.cookie.set('cookie-disclaimer', 'policy-accepted', this.cookieTime);
        }
    }
};

UTILS.init();

//upload logo file
const UploadFile = (() => {
    const uploadLogoInputs = Array.prototype.slice.call(document.querySelectorAll('.logo-file'));
    if (!uploadLogoInputs) {
        return;
    }

    uploadLogoInputs.map((input) => {
        const sibling = input.parentNode.nextSibling.nextSibling;
        input.addEventListener('change', function() {
            const { files } = this;
            sibling.querySelector('.fileitem__item').innerHTML = files[0].name;
        });

        const delImage =  Array.prototype.slice.call(document.querySelectorAll('.del-image'));

        delImage.map(function (del) {
            del.onclick = function (e) {
                const click = e.target;

                click.parentNode.querySelector('.fileitem__item').innerHTML = '';
                const inputfile = click.parentNode.parentNode.querySelector('input[type="file"]');
                inputfile.file = '';
            };
        });
    });

});
UploadFile();

//del piwo
const UpdateDelPiwo = (() => {
    const delPiwo = Array.prototype.slice.call(document.querySelectorAll('.del-piwo'));

    delPiwo.map((del) => {
        del.onclick = function (e) {
            document.querySelector('.beersinfosection').removeChild(e.target.parentNode.parentNode.parentNode.parentNode);
        };
    });
});
UpdateDelPiwo();

//create new beer
(() => {
    const createBeerBtn = document.querySelector('#createbeersection');

    if (!createBeerBtn) {
        return;
    }

    createBeerBtn.addEventListener('click', function(e) {
        const countSection = document.querySelectorAll('.beerinfosection').length;
        const nextBeer = document.createRange().createContextualFragment(createTemplate(countSection));
        e.preventDefault();
        document.querySelector('.beersinfosection').appendChild(nextBeer);
        InputListener();
        UploadFile();
        UpdateDelPiwo();
    });

    const createTemplate = (no) => {
        return `
            <div class="beerinfosection" id="beerinfosection_${no}">
                <div class="main-step-header">
                    <div class="step-header mb-18">
                        <h3 class="main-step__header">Piwo <img class="del-piwo" src="includes/images/ico-close.png" alt="" /></h3>
                    </div>
                </div>
                <div class="form-date form-style mb-12">
                    <div class="dropdowns">
                        <div class="dropdown">
                            <select class="dropdown" name="" id="style_${no}">
                                <option value="styl1">styl1</option>
                                <option value="styl2">styl2</option>
                                <option value="styl3">styl3</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="insert-more">
                    <label for="moredesc_${no}">
                        <span>Opis stylu (opcjonalnie)
                            maks. 1000 znaków</span>
                        </label>
                    <input type="checkbox" class="insert-more__btn" id="moredesc_${no}" />
                    <textarea name="" id="desc_${no}" cols="30" rows="10" class="textarea"></textarea>
                </div>
                <div class="form-section">
                    <input type="text" class="form-control">
                    <label>Nazwa piwa</label>
                </div>
                <div class="form-section">
                    <input type="text" class="form-control">
                    <label>Wysokość ekstraktu BLG</label>
                </div>
                <div class="form-section">
                    <input type="text" class="form-control">
                    <label>Zawartość alkoholu (obj.)</label>
                </div>
                <div class="form-date mb-12">
                    <span class="form-date__header">Data warzenia:</span>
                    <div class="dropdowns">
                        <div class="dropdown dropdown-1">
                            <select class="dropdown " name="" id="day_${no}">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <div class="dropdown dropdown-2">
                            <select class="dropdown" name="" id="month_${no}">
                                <option value="0">Styczeń</option>
                                <option value="1">Luty</option>
                                <option value="2">Marzec</option>
                                <option value="3">Kwiecień</option>
                                <option value="4">Maj</option>
                                <option value="5">Czerwiec</option>
                                <option value="6">Lipiec</option>
                                <option value="7">Sierpień</option>
                                <option value="8">Wrzesień</option>
                                <option value="9">Październik</option>
                                <option value="10">Listopad</option>
                                <option value="11">Grudzień</option>
                            </select>
                        </div>
                        <div class="dropdown dropdown-3">
                            <select class="dropdown" name="" id="year_${no}">
                                <option value="2018">2018</option>
                                <option value="2017">2017</option>
                                <option value="2016">2016</option>
                                <option value="2015">2015</option>
                                <option value="2014">2014</option>
                                <option value="2013">2013</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="insert-more">
                    <label for="more_${no}">
                        <span>Opis stylu (opcjonalnie)
                            maks. 1000 znaków</span>
                        </label>
                    <input type="checkbox" class="insert-more__btn" id="more_${no}" />
                    <textarea name="" id="des_${no}" cols="30" rows="10" class="textarea"></textarea>
                </div>
                <div class="insert-more insert-more--file mb-13">
                    <label for="logo-file_${no}">
                        <span>Etykieta/logo browaru (opcjonalnie) plik JPG lub PNG maks. 2 MB</span>
                        <input type="file" id="logo-file_${no}" class="logo-file" name="file"  />
                    </label>
                    <div class="fileitem">
                        <span class="fileitem__item"></span>
                        <img class="del-image" src="includes/images/ico-close.png" alt="" />
                    </div>
                </div>
            </div>
        `;
    };
})();