'use strict';
//header menu

(function () {
    var headerMenuBtn = document.querySelector('.header__menu__hamburger');
    headerMenuBtn.addEventListener('click', function () {
        headerMenuBtn.classList.toggle('header__menu__hamburger--open');
    });
})();

//inputs active
var InputListener = function InputListener() {
    var allInput = Array.prototype.slice.call(document.querySelectorAll('.form-control'));
    if (!allInput) {
        return;
    }

    allInput.map(function (input) {
        input.addEventListener('change', function (e) {
            var target = e.target;

            if (!target.value) {
                if (!target.classList.contains('active')) {
                    return;
                }
                target.classList.remove('active');
            } else {
                target.classList.add('active');
            }
        });
    });
};

InputListener();

//auto submit
var form = document.getElementById('filters');

if (form != null) {
    form.addEventListener('change', function () {
        form.submit();
    });
}

var UTILS = {
    init: function init() {
        //defaultowe klasy które zawsze muszą być uruchomione
        this.cookieDisclaimer.init();
    },
    hasClass: function hasClass(ele, cls) {
        return !!ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
    },
    addClass: function addClass(ele, cls) {
        if (!UTILS.hasClass(ele, cls)) {
            ele.className += ' ' + cls;
        }
    },
    removeClass: function removeClass(ele, cls) {
        if (UTILS.hasClass(ele, cls)) {
            var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
            ele.className = ele.className.replace(reg, ' ');
        }
    },
    cookie: {
        set: function set(c_name, value, exdays) {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + exdays);
            var c_value = escape(value) + (exdays == null ? '' : '; path=/; expires=' + exdate.toUTCString());
            document.cookie = c_name + '=' + c_value;
        },
        get: function get(c_name) {
            var i = void 0,
                x = void 0,
                y = void 0,
                ARRcookies = document.cookie.split(';');
            for (i = 0; i < ARRcookies.length; i++) {
                x = ARRcookies[i].substr(0, ARRcookies[i].indexOf('='));
                y = ARRcookies[i].substr(ARRcookies[i].indexOf('=') + 1);
                x = x.replace(/^\s+|\s+$/g, '');
                if (x == c_name) {
                    return unescape(y);
                }
            }
        }
    },
    cookieDisclaimer: {
        cookieTime: 360, //in days
        container: null,
        disclaimerText: 'Informujemy, że w Portalu Birofilia.org wykorzystujemy pliki cookies. Modyfikowanie, blokowanie i usuwanie plików cookies możliwe jest przy użyciu przeglądarki internetowej. Więcej informacji na temat plików cookies można znaleźć w <a href="#">Polityce cookie Birofilia.org.</a>',
        disclaimerClose: 'OK',
        init: function init() {
            var that = this;
            if (UTILS.cookie.get('cookie-disclaimer')) {
                that.setCookie();
            } else {
                document.body.innerHTML += '<div id="cookie-disclaimer"><div class="cookie-disclaimer-wrapper"><button class="cookie-disclaimer-close">' + this.disclaimerClose + '</button><div class="cookie-disclaimer-content">' + this.disclaimerText + '</div></div></div>';
                document.getElementsByClassName('cookie-disclaimer-close')[0].addEventListener('click', function () {
                    that.close();
                });

                UTILS.addClass(document.getElementById('cookie-disclaimer'), 'active');
            }
        },
        close: function close() {
            this.setCookie();
            UTILS.removeClass(document.getElementById('cookie-disclaimer'), 'active');
        },
        setCookie: function setCookie() {
            UTILS.cookie.set('cookie-disclaimer', 'policy-accepted', this.cookieTime);
        }
    }
};

UTILS.init();

//upload logo file
var UploadFile = function UploadFile() {
    var uploadLogoInputs = Array.prototype.slice.call(document.querySelectorAll('.logo-file'));
    if (!uploadLogoInputs) {
        return;
    }

    uploadLogoInputs.map(function (input) {
        var sibling = input.parentNode.nextSibling.nextSibling;
        input.addEventListener('change', function () {
            var files = this.files;

            sibling.querySelector('.fileitem__item').innerHTML = files[0].name;
        });

        var delImage = Array.prototype.slice.call(document.querySelectorAll('.del-image'));

        delImage.map(function (del) {
            del.onclick = function (e) {
                var click = e.target;

                click.parentNode.querySelector('.fileitem__item').innerHTML = '';
                var inputfile = click.parentNode.parentNode.querySelector('input[type="file"]');
                inputfile.file = '';
            };
        });
    });
};
UploadFile();

//del piwo
var UpdateDelPiwo = function UpdateDelPiwo() {
    var delPiwo = Array.prototype.slice.call(document.querySelectorAll('.del-piwo'));

    delPiwo.map(function (del) {
        del.onclick = function (e) {
            document.querySelector('.beersinfosection').removeChild(e.target.parentNode.parentNode.parentNode.parentNode);
        };
    });
};
UpdateDelPiwo();

//create new beer
(function () {
    var createBeerBtn = document.querySelector('#createbeersection');

    if (!createBeerBtn) {
        return;
    }

    createBeerBtn.addEventListener('click', function (e) {
        var countSection = document.querySelectorAll('.beerinfosection').length;
        var nextBeer = document.createRange().createContextualFragment(createTemplate(countSection));
        e.preventDefault();
        document.querySelector('.beersinfosection').appendChild(nextBeer);
        InputListener();
        UploadFile();
        UpdateDelPiwo();
    });

    var createTemplate = function createTemplate(no) {
        return '\n            <div class="beerinfosection" id="beerinfosection_' + no + '">\n                <div class="main-step-header">\n                    <div class="step-header mb-18">\n                        <h3 class="main-step__header">Piwo <img class="del-piwo" src="includes/images/ico-close.png" alt="" /></h3>\n                    </div>\n                </div>\n                <div class="form-date form-style mb-12">\n                    <div class="dropdowns">\n                        <div class="dropdown">\n                            <select class="dropdown" name="" id="style_' + no + '">\n                                <option value="styl1">styl1</option>\n                                <option value="styl2">styl2</option>\n                                <option value="styl3">styl3</option>\n                            </select>\n                        </div>\n                    </div>\n                </div>\n                <div class="insert-more">\n                    <label for="moredesc_' + no + '">\n                        <span>Opis stylu (opcjonalnie)\n                            maks. 1000 znak\xF3w</span>\n                        </label>\n                    <input type="checkbox" class="insert-more__btn" id="moredesc_' + no + '" />\n                    <textarea name="" id="desc_' + no + '" cols="30" rows="10" class="textarea"></textarea>\n                </div>\n                <div class="form-section">\n                    <input type="text" class="form-control">\n                    <label>Nazwa piwa</label>\n                </div>\n                <div class="form-section">\n                    <input type="text" class="form-control">\n                    <label>Wysoko\u015B\u0107 ekstraktu BLG</label>\n                </div>\n                <div class="form-section">\n                    <input type="text" class="form-control">\n                    <label>Zawarto\u015B\u0107 alkoholu (obj.)</label>\n                </div>\n                <div class="form-date mb-12">\n                    <span class="form-date__header">Data warzenia:</span>\n                    <div class="dropdowns">\n                        <div class="dropdown dropdown-1">\n                            <select class="dropdown " name="" id="day_' + no + '">\n                                <option value="1">1</option>\n                                <option value="2">2</option>\n                                <option value="3">3</option>\n                                <option value="4">4</option>\n                                <option value="5">5</option>\n                                <option value="6">6</option>\n                                <option value="7">7</option>\n                                <option value="8">8</option>\n                            </select>\n                        </div>\n                        <div class="dropdown dropdown-2">\n                            <select class="dropdown" name="" id="month_' + no + '">\n                                <option value="0">Stycze\u0144</option>\n                                <option value="1">Luty</option>\n                                <option value="2">Marzec</option>\n                                <option value="3">Kwiecie\u0144</option>\n                                <option value="4">Maj</option>\n                                <option value="5">Czerwiec</option>\n                                <option value="6">Lipiec</option>\n                                <option value="7">Sierpie\u0144</option>\n                                <option value="8">Wrzesie\u0144</option>\n                                <option value="9">Pa\u017Adziernik</option>\n                                <option value="10">Listopad</option>\n                                <option value="11">Grudzie\u0144</option>\n                            </select>\n                        </div>\n                        <div class="dropdown dropdown-3">\n                            <select class="dropdown" name="" id="year_' + no + '">\n                                <option value="2018">2018</option>\n                                <option value="2017">2017</option>\n                                <option value="2016">2016</option>\n                                <option value="2015">2015</option>\n                                <option value="2014">2014</option>\n                                <option value="2013">2013</option>\n                            </select>\n                        </div>\n                    </div>\n                </div>\n                <div class="insert-more">\n                    <label for="more_' + no + '">\n                        <span>Opis stylu (opcjonalnie)\n                            maks. 1000 znak\xF3w</span>\n                        </label>\n                    <input type="checkbox" class="insert-more__btn" id="more_' + no + '" />\n                    <textarea name="" id="des_' + no + '" cols="30" rows="10" class="textarea"></textarea>\n                </div>\n                <div class="insert-more insert-more--file mb-13">\n                    <label for="logo-file_' + no + '">\n                        <span>Etykieta/logo browaru (opcjonalnie) plik JPG lub PNG maks. 2 MB</span>\n                        <input type="file" id="logo-file_' + no + '" class="logo-file" name="file"  />\n                    </label>\n                    <div class="fileitem">\n                        <span class="fileitem__item"></span>\n                        <img class="del-image" src="includes/images/ico-close.png" alt="" />\n                    </div>\n                </div>\n            </div>\n        ';
    };
})();